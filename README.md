# Three To Two Merge Use Cases

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DITA Merge release.*
*The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-DITA-Merge-x_y_z_j/samples/sample-name`.* 

------


Three way merging is used when two files are modified with respect to a common ancestor version. If there are only three versions or inputs involved in a merge,
then merge result can be defined as adds and deletes relative to the two branches. This is the main concept for three to two merge.

For full information about the formatting elements sample see our web page [Three To Two Merge Use Cases](https://docs.deltaxml.com/dita-merge/latest/samples-and-guides/three-to-two-merge-use-cases) 

## Running the Sample

If you have [Ant](https://ant.apache.org) installed, use the build script provided to run the sample.
Simply type the following command to run the pipeline and produce the output file inside the output directory.

    ant run
	
This will generate result files in the output directory.

These results can also be generated separately for each case by running the following commands:

    ant all-changes
	
    ant conflicting-changes
	
    ant their-changes
	
### Running from command-line

Alternatively, the following command can be used to run the above samples from a command-line, replacing x.y.z with the major.minor.patch version number of your release e.g. deltaxml-dita-merge-3.0.0.jar:

    java -jar deltaxml-dita-merge-x.y.z.jar merge concurrent3 ancestorName ancestorFile version1Name version1File version2Name version2File resultFile ResultType=value
	
The parameter *ResultType* value should be either `ALL_CHANGES`, `CONFLICTING_CHANGES` or `THEIR_CHANGES`.